package com.java.exam.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

import com.java.exam.model.Name;

public class NameTest {
	
	@Ignore
	@Test
	public void test_firstName(){
		Name getFirstName = new Name("First", "", "");
		assertEquals(getFirstName.getFirst(), "First");
	}
	
	@Ignore
	@Test
	public void test_middleName(){
		Name getMiddleName = new Name("First", "Middle", "Last");
		assertTrue(getMiddleName.getMiddle()=="Middle");
	}
	
	@Ignore
	@Test
	public void test_emptyName(){
		Name getName = new Name("", "", "");
		assertTrue(getName!=null);
	}
	
}
