package com.java.exam.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.exam.exception.DataNotFoundException;
import com.java.exam.manager.ProfileManager;
import com.java.exam.model.Profile;

public class ProfileManagerTest {
	public static final String PROFILE_LIST_URI = "http://s3-ap-southeast-1.amazonaws.com/fundo/js/profiles.json";

	@Ignore
	@Test
	public void test_getUrlContent() throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper objectMapper = new ObjectMapper();
		URL url = new URL(PROFILE_LIST_URI);
		ArrayList<Profile> profiles = objectMapper.readValue(url, new TypeReference<ArrayList<Profile>>() {});
		assertTrue(profiles!=null);
	}
	
	@Ignore
	@Test(expected=Exception.class)
	public void test_checkUrlContent() throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper objectMapper = new ObjectMapper();
		URL url = new URL("http://DummyUri");
		ArrayList<Profile> profiles = objectMapper.readValue(url, new TypeReference<ArrayList<Profile>>() {});
		assertEquals(profiles, null);
	}
	
	@Ignore
	@Test
	public void test_getProfile() throws JsonParseException, JsonMappingException, IOException, DataNotFoundException{
		Profile profile = ProfileManager.getProfile(PROFILE_LIST_URI, new Profile("59914e16322c1b042d4fdf2b"));
		assertEquals(profile, new Profile("59914e16322c1b042d4fdf2b"));
	}
	
	@Ignore
	@Test
	public void test_getProfileDetails() throws JsonParseException, JsonMappingException, IOException, DataNotFoundException{
		Profile profile = ProfileManager.getProfile(PROFILE_LIST_URI, new Profile("59914e16322c1b042d4fdf2b"));
		assertTrue(profile != new Profile("59914e1633eb7f7472171dd3"));
	}

}
