package com.java.exam.controller;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.java.exam.exception.DataNotFoundException;
import com.java.exam.manager.ProfileManager;
import com.java.exam.model.Profile;
 
@Controller
public class ProfileController {

	public static final String PROFILE_LIST_URI = "http://s3-ap-southeast-1.amazonaws.com/fundo/js/profiles.json";

	@RequestMapping("/")
	public ModelAndView homePage() {
		ModelAndView modelAndView = new ModelAndView("index");
		try {
			ArrayList<Profile> profiles = ProfileManager.getProfileList(PROFILE_LIST_URI);
			modelAndView.addObject("profiles", profiles);
			modelAndView.addObject("status_description", "success");
			modelAndView.addObject("status_code", "200");
		} catch (Exception e) {
			System.out.println("ERROR : " + e);
			modelAndView.addObject("status_description", "Internal Error");
			modelAndView.addObject("status_code", "401");
		}

		return modelAndView;
	}

	@RequestMapping(value = "/view")
	public ModelAndView profilePage(HttpServletRequest request) {
		String id = request.getParameter("id");
		ModelAndView modelAndView = new ModelAndView("profile");
		try {
			Profile profile = ProfileManager.getProfile(PROFILE_LIST_URI, new Profile(id));
			modelAndView.addObject("profile", profile);
			modelAndView.addObject("status_description", "success");
			modelAndView.addObject("status_code", "200");
		} catch (DataNotFoundException e) {
			System.out.println("ERROR : " + e);
			modelAndView.addObject("status_description", "Data Not Found");
			modelAndView.addObject("status_code", "400");
		} catch (Exception e) {
			System.out.println("ERROR : " + e);
			modelAndView.addObject("status_description", "Internal Error");
			modelAndView.addObject("status_code", "401");
		}
		return modelAndView;
	}

}
