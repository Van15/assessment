package com.java.exam.manager;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.java.exam.exception.DataNotFoundException;
import com.java.exam.model.Profile;

public class ProfileManager {
	
	public static Profile getProfile(String uri, Profile profile)
			throws JsonParseException, JsonMappingException, IOException, DataNotFoundException  {
		
		ArrayList<Profile> profiles = getProfileList(uri);
		for (Profile p : profiles) {
			if (profile.equals(p))
				return p;
		}
	
		throw new DataNotFoundException("Data not found.");

	}

	public static ArrayList<Profile> getProfileList(String uri)
			throws JsonParseException, JsonMappingException, IOException {
		
		ObjectMapper objectMapper = new ObjectMapper();
		URL url = new URL(uri);
		ArrayList<Profile> profiles = objectMapper.readValue(url, new TypeReference<ArrayList<Profile>>() {
		});
		return profiles;
	}
}

	
