<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<script>
$(document).ready(function() {
    $('#customers').DataTable();
} );

</script>

</head>
<body>
	<div class="container">

		<c:choose>
			<c:when test="${status_code eq '401' }">
				<p>${status_desc }</p>
			</c:when>

			<c:otherwise>
				<table id="customers" class="display" style="width:100%">
					<thead>
						<tr>
							<th>Name</th>
							<th>Age</th>
							<th>Active</th>
							<th>Blocked</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${profiles }" var="profile">
							<tr
								onclick="window.location='http://localhost:8080/profile/view?id=${profile.id}';">
								<td>${profile.name.wholeName }</td>
								<td>${profile.age}</td>
								<td>${profile.active eq true ? '&#9745;' :  '&#9744;'} </td>
								<td>${profile.blocked eq true ? '&#9745;' :  '&#9744;'}</td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</c:otherwise>
		</c:choose>


	</div>

</body>
</html>
