<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Profile</title>


</head>
<body>

	<c:choose>
		<c:when test="${status_code eq '200' }">
			<div class="container">
				<div class="container1">
					<div class="profile">
						<img src="${profile.picture }" alt="Avatar" style="width: 100%">

					</div>
					<div class="container2">
						<h4>
							<b>${profile.name.wholeName }</b>
						</h4>
					</div>
				</div>

				<br> <br>
				<div class="container2">

					<table>
						<tr>
							<td>ID:</td>
							<td><b>${profile.id }</b></td>
						</tr>
						<tr>
							<td valign="top">Profile:</td>
							<td><b>${profile.profile }</b></td>
						</tr>
						<tr>
							<td>Email:</td>
							<td><b>${profile.email }</b></td>
						</tr>
						<tr>
							<td>Phone:</td>
							<td><b>${profile.phone }</b></td>
						</tr>
						<tr>
							<td>Address:</td>
							<td><b>${profile.address }</b></td>
						</tr>
						<tr>
							<td>Age:</td>
							<td><b>${profile.age }</b></td>
						</tr>

						<tr>
							<td>Balance:</td>
							<td><b>${profile.balance }</b></td>
						</tr>

					</table>

				</div>
			</div>

		</c:when>

		<c:otherwise>
			<p>${status_desc }</p>
		</c:otherwise>
	</c:choose>

</body>
</html>


<style>
.profile {
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
	transition: 0.3s;
	width: 100px;
}

.card:hover {
	box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
}

.container {
	padding: 2px 16px;
	border: 1px solid black;
}

.container1 {
	padding: 16px 16px;
	display: inline-flex;
}

.container2 {
	padding: 2px 16px;
}
</style>